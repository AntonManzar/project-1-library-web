package libraryBook.dao;

import libraryBook.models.Book;
import libraryBook.models.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PatchMapping;

import java.util.List;
import java.util.Optional;

@Component
public class LibraryDAO {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public LibraryDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Book> showListBook() {
        return jdbcTemplate.query("select * from book", new LibraryMapper());
    }

    public Book showBook(int bookId) {
        return jdbcTemplate.query("select * from book where book_id=?", new Object[]{bookId},
                new LibraryMapper()).stream().findAny().orElse(null);
    }

    public Optional<Book> showBook(String title) {
        return jdbcTemplate.query("SELECT * FROM book WHERE title=?", new Object[]{title},
                new LibraryMapper()).stream().findAny();
    }

    public void createNewBook(Book book) {
        jdbcTemplate.update(
                "insert into book ( author, title, year_of_publishing) values (?,?,?);",
                book.getAuthor(), book.getTitle(), book.getYearOfPublishing());

    }

    public void editBook(int bookId, Book editBook) {
        jdbcTemplate.update("UPDATE book SET author=?,title=?, year_of_publishing=? WHERE book_id=?", editBook.getAuthor(),
                editBook.getTitle(), editBook.getYearOfPublishing(), bookId);
    }

    public void deleteBook(int bookId) {
        jdbcTemplate.update("DELETE FROM book WHERE book_id=?", bookId);
    }
}
