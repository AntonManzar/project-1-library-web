package libraryBook.dao;

import org.springframework.jdbc.core.RowMapper;
import libraryBook.models.Person;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonMapper implements RowMapper<Person> {

    @Override
    public Person mapRow(ResultSet resultSet, int i) throws SQLException {
        Person person = new Person();

        person.setPersonId(resultSet.getInt("person_id"));
        person.setName(resultSet.getString("name"));
        person.setYearOfBirth(resultSet.getInt("year_of_birth"));

        return person;
    }
}
