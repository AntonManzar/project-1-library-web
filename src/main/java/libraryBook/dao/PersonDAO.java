package libraryBook.dao;

import libraryBook.models.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class PersonDAO {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public PersonDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Person> showListPeople() {
        return jdbcTemplate.query("SELECT * FROM person", new BeanPropertyRowMapper<>(Person.class));
    }

    public Person showPerson(int personId) {
        return jdbcTemplate.query("SELECT * FROM person WHERE person_id=?", new Object[]{personId},
                new BeanPropertyRowMapper<>(Person.class)).stream().findAny().orElse(null);
    }

    public Optional<Person> showPerson(String name) {
        return jdbcTemplate.query("SELECT * FROM person WHERE name=?", new Object[]{name},
                new BeanPropertyRowMapper<>(Person.class)).stream().findAny();
    }

    public void createNewPerson(Person person) {
        jdbcTemplate.update("INSERT INTO person (name, year_of_birth) VALUES (?,?)",
                person.getName(), person.getYearOfBirth());
    }

    public void editPerson(int personId, Person editPerson) {
        jdbcTemplate.update("UPDATE person SET name=?,year_of_birth=? WHERE person_id=?", editPerson.getName(),
                editPerson.getYearOfBirth(), personId);
    }

    public void deletePerson(int personId) {
        jdbcTemplate.update("DELETE FROM person WHERE person_id=?", personId);
    }
}
