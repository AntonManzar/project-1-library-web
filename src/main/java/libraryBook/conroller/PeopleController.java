package libraryBook.conroller;

import libraryBook.dao.PersonDAO;
import libraryBook.models.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import libraryBook.util.PersonValidator;

import javax.validation.Valid;

@Controller
@RequestMapping("/people")
public class PeopleController {

    private final PersonDAO personDAO;
    private final PersonValidator personValidator;

    @Autowired
    public PeopleController(PersonDAO personDAO, PersonValidator personValidator) {
        this.personDAO = personDAO;
        this.personValidator = personValidator;
    }

    @GetMapping
    public String showListPeople(Model model) {
        model.addAttribute("people", personDAO.showListPeople());

        return "people/showListPeople";
    }

    @GetMapping("/{person_id}")
    public String showPerson(@PathVariable("person_id") int personId, Model model) {
        model.addAttribute("person", personDAO.showPerson(personId));

        return "people/showPerson";
    }

    @GetMapping("/createNewPerson")
    public String createNewPerson(@ModelAttribute("person") Person person) {

        return "people/createNewPerson";
    }

    @PostMapping
    public String createPerson(@ModelAttribute("person") @Valid Person person, BindingResult bindingResult) {
        personValidator.validate(person, bindingResult);
        if (bindingResult.hasErrors()) return "people/createNewPerson";

        personDAO.createNewPerson(person);

        return "redirect:/people";
    }

    @GetMapping("/{person_id}/editPerson")
    public String editPersonGet(@PathVariable("person_id") int personId, Model model) {
        model.addAttribute("person", personDAO.showPerson(personId));

        return "people/editPerson";
    }

    @PatchMapping("/{person_id}")
    public String editPersonPatch(@ModelAttribute("person") @Valid Person person, BindingResult bindingResult,
                                  @PathVariable("person_id") int personId) {

        personValidator.validate(person, bindingResult);
        if (bindingResult.hasErrors()) return "people/editPerson";

        personDAO.editPerson(personId, person);

        return "redirect:/people";
    }

    @DeleteMapping("/{person_id}")
    public String deletePerson(@PathVariable("person_id") int personId) {
        personDAO.deletePerson(personId);
        return "redirect:/people";
    }
}
