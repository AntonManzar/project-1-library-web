package libraryBook.conroller;

import libraryBook.dao.LibraryDAO;
import libraryBook.models.Book;
import libraryBook.models.Person;
import libraryBook.util.BookValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/library")
public class LibraryController {

    private final LibraryDAO libraryDAO;
    private final BookValidator bookValidator;

    @Autowired
    public LibraryController(LibraryDAO libraryDAO, BookValidator bookValidator) {
        this.libraryDAO = libraryDAO;
        this.bookValidator = bookValidator;
    }

    @GetMapping
    public String showListBook(Model model) {
        model.addAttribute("library", libraryDAO.showListBook());

        return "library/showListBook";
    }

    @GetMapping("/{book_id}")
    public String showBook(@PathVariable("book_id") int bookId, Model model) {
        model.addAttribute("book", libraryDAO.showBook(bookId));

        return "library/showBook";
    }

    @GetMapping("/createNewBook")
    public String createNewBook(@ModelAttribute("book") Book book) {

        return "library/createNewBook";
    }

    @PostMapping
    public String createBook(@ModelAttribute("book") @Valid Book book, BindingResult bindingResult) {
        bookValidator.validate(book, bindingResult);
        if (bindingResult.hasErrors()) return "library/createNewBook";

        libraryDAO.createNewBook(book);

        return "redirect:/library";
    }

    @GetMapping("/{book_id}/editBook")
    public String editBookGet(@PathVariable("book_id") int bookId, Model model) {
        model.addAttribute("book", libraryDAO.showBook(bookId));

        return "library/editBook";
    }

    @PatchMapping("/{book_id}")
    public String editBookPatch(@ModelAttribute("book") @Valid Book book, BindingResult bindingResult,
                                @PathVariable("book_id") int bookId) {

        bookValidator.validate(book, bindingResult);
        if (bindingResult.hasErrors()) return "library/editBook";

        libraryDAO.editBook(bookId, book);

        return "redirect:/library";
    }

    @DeleteMapping("/{book_id}")
    public String deletePerson(@PathVariable("book_id") int bookId) {
        libraryDAO.deleteBook(bookId);
        return "redirect:/library";
    }
}

















