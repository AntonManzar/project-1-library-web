package libraryBook.util;

import libraryBook.dao.LibraryDAO;
import libraryBook.models.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class BookValidator implements Validator {
    private final LibraryDAO libraryDAO;

    @Autowired
    public BookValidator(LibraryDAO libraryDAO) {
        this.libraryDAO = libraryDAO;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return Book.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Book book = (Book) o;

        if (libraryDAO.showBook(book.getTitle()).isPresent())
            errors.rejectValue("title", "", "Это название уже есть!");
    }
}
