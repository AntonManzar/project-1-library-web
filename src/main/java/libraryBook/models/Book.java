package libraryBook.models;

import javax.validation.constraints.*;

public class Book {
    private int personId;
    private int bookId;

    @NotEmpty(message = "Нужно ввести название!")
    @Size(min = 2, max = 50, message = "Название должно быть в диапазоне от 2 до 50 символов")
    private String title;

    @Min(value = 0, message ="Год должен быть валидным!")
    private int yearOfPublishing;

    @NotEmpty(message = "Нужно ввести автора!")
    @Size(min = 2, max = 50, message = "Имя автора должно быть в диапазоне от 2 до 50 символов")
    private String author;

    public Book() {
    }

    public Book(String title, int yearOfPublishing, String author) {
        this.title = title;
        this.yearOfPublishing = yearOfPublishing;
        this.author = author;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYearOfPublishing() {
        return yearOfPublishing;
    }

    public void setYearOfPublishing(int yearOfPublishing) {
        this.yearOfPublishing = yearOfPublishing;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }
}
