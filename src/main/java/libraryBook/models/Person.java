package libraryBook.models;

import javax.validation.constraints.*;

public class Person {

    private int personId;
    @NotEmpty(message = "Поле для мени не может быть пустым")
    @Size(max = 50, message = "Имя не должно превышать 50 символов!")
    @Pattern(regexp = "[А-Я][а-я]+ [А-Я][а-я]+ [А-Я][а-я]+",
            message = "Имя должно быть формата ФИО и начинаться с заглавной буквы! Пример: Иванов Иван Иванович")
    private String name;

    @Min(value = 1900, message = "Год не может быть указан раньше чем 1900!")
    private int yearOfBirth;

    public Person() {
    }

    public Person(String name, int yearOfBirth) {
        this.name = name;
        this.yearOfBirth = yearOfBirth;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }
}
