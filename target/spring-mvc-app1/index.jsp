﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Библиотека. Главная страница</title>
</head>
<body style="text-align: center">
<hr/>
<br>
<h1>Библиотека</h1>
<br>
<hr/>
<br>
<h2>
    <a href="/people">Список всех посетителей библиотеки</a>
    <br>
    <br>
    <a href="/people/createNewPerson">Добавить нового посетителя!</a>
    <br>
    <hr/>
    <br>
    <a href="/library">Список всех книг библиотеки</a>
    <br>
    <br>
    <a href="/library/createNewBook">Добавить новую книгу!</a>
    <br>
    <hr/>
    <br>
</h2>
</body>
</html>
